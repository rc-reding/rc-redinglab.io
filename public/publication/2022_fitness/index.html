<!DOCTYPE html>
<html lang="en-us">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="generator" content="Source Themes Academia 4.3.1">
  <meta name="generator" content="Hugo 0.124.1">

  

  
  
  
  
  
    
    
    
  
  

  <meta name="author" content="Carlos Reding">

  
  
  
    
  
  <meta name="description" content="The link between fitness and reproduction rate is a central tenet in evolutionary biology: mutants reproducing faster than the dominant wild-type are favoured by selection, otherwise the mutation is lost. This link is given by Fisher&#39;s theorem under the assumption that fitness can only change through mutations. Here I show that fitness, as formalised by Fisher, changes through time without invoking new mutations---allowing the co-maintenance of fast- and slow-growing genotypes. The theorem does not account for changes in population growth that naturally occur due to resource depletion, but it is key to this unforeseen co-maintenance of growth strategies. To demonstrate that fitness is not constant, as Fisher&#39;s theorem predicates, I co-maintained a construct of _Escherichia coli_ harbouring pGW155B, a non-transmissible plasmid that protects against tetracycline, in competition assays without using antibiotics. Despite growing 40% slower than its drug-sensitive counterpart, the construct with pGW155B persisted throughout the competition without tetracyclin---maintaining the plasmid. Thus, predicting the selection of microbial genotypes may be more difficult than previously thought.">

  
  <link rel="alternate" hreflang="en-us" href="https://rc-reding.gitlab.io/publication/2022_fitness/">

  


  

  
  
  
  <meta name="theme-color" content="#fc6f5c">
  

  
  
  
  
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/academicons/1.8.6/css/academicons.min.css" integrity="sha256-uFVgMKfistnJAfoCUQigIl+JfUaP47GrRKjf6CTPVmw=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" integrity="sha256-ygkqlh3CYSUri3LhQxzdcm0n1EQvH2Y+U5S2idbLtxs=" crossorigin="anonymous">

    
    
    
      
    
    
      
      
        
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/github.min.css" crossorigin="anonymous" title="hl-light">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/dracula.min.css" crossorigin="anonymous" title="hl-dark" disabled>
        
      
    

    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/leaflet.css" integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ==" crossorigin="anonymous">
    

    

  

  
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Overpass+Mono|Playfair+Display:400,900|PT+Sans:400,700&display=swap">
  

  
  
  
  <link rel="stylesheet" href="/css/academia.min.47598c25f597d7aa5d83f45a44173a9b.css">

  

  
  
  
  

  

  <link rel="manifest" href="/site.webmanifest">
  <link rel="icon" type="image/png" href="/img/icon.png">
  <link rel="apple-touch-icon" type="image/png" href="/img/icon-192.png">

  <link rel="canonical" href="https://rc-reding.gitlab.io/publication/2022_fitness/">

  
  
  
  
    
  
  <meta property="twitter:card" content="summary_large_image">
  
  <meta property="twitter:site" content="@rc_reding">
  <meta property="twitter:creator" content="@rc_reding">
  
  <meta property="og:site_name" content="R3C">
  <meta property="og:url" content="https://rc-reding.gitlab.io/publication/2022_fitness/">
  <meta property="og:title" content="Plasmid carriage and the unorthodox use of Fisher&#39;s theorem in evolutionary biology | R3C">
  <meta property="og:description" content="The link between fitness and reproduction rate is a central tenet in evolutionary biology: mutants reproducing faster than the dominant wild-type are favoured by selection, otherwise the mutation is lost. This link is given by Fisher&#39;s theorem under the assumption that fitness can only change through mutations. Here I show that fitness, as formalised by Fisher, changes through time without invoking new mutations---allowing the co-maintenance of fast- and slow-growing genotypes. The theorem does not account for changes in population growth that naturally occur due to resource depletion, but it is key to this unforeseen co-maintenance of growth strategies. To demonstrate that fitness is not constant, as Fisher&#39;s theorem predicates, I co-maintained a construct of _Escherichia coli_ harbouring pGW155B, a non-transmissible plasmid that protects against tetracycline, in competition assays without using antibiotics. Despite growing 40% slower than its drug-sensitive counterpart, the construct with pGW155B persisted throughout the competition without tetracyclin---maintaining the plasmid. Thus, predicting the selection of microbial genotypes may be more difficult than previously thought."><meta property="og:image" content="https://rc-reding.gitlab.io/publication/2022_fitness/featured.jpg">
  <meta property="twitter:image" content="https://rc-reding.gitlab.io/publication/2022_fitness/featured.jpg"><meta property="og:locale" content="en-us">
  
  <meta property="article:published_time" content="2017-01-01T00:00:00&#43;00:00">
  
  <meta property="article:modified_time" content="2022-02-22T00:00:00&#43;00:00">
  

  


  





  <title>Plasmid carriage and the unorthodox use of Fisher&#39;s theorem in evolutionary biology | R3C</title>

</head>


<body id="top" data-spy="scroll" data-target="#TableOfContents" data-offset="71" >

  <aside class="search-results" id="search">
  <div class="container">
    <section class="search-header">

      <div class="row no-gutters justify-content-between mb-3">
        <div class="col-6">
          <h1>Search</h1>
        </div>
        <div class="col-6 col-search-close">
          <a class="js-search" href="#"><i class="fas fa-times-circle text-muted" aria-hidden="true"></i></a>
        </div>
      </div>

      <div id="search-box">
        
        
        
      </div>

    </section>
    <section class="section-search-results">

      <div id="search-hits">
        
      </div>

    </section>
  </div>
</aside>


  
<nav class="navbar navbar-light fixed-top navbar-expand-lg py-0" id="navbar-main">
  <div class="container">

    
      <a class="navbar-brand" href="/"><img src="/img/ndm_logo.png" alt="R3C"></a>
      
      <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation"><span><i class="fas fa-bars"></i></span>
      </button>
      

    
    <div class="collapse navbar-collapse" id="navbar">
      
      
      <ul class="navbar-nav ml-auto">
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#about"><span>Home</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#research"><span>Research</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#publications"><span>Publications</span></a>
        </li>

        
        

        

        
        
        
          
        

        
        
        
        
        
        
          
          
          
            
          
          
        

        <li class="nav-item">
          <a class="nav-link " href="/#contact"><span>Contact</span></a>
        </li>

        
        

      

        

        

        

        
        <li class="nav-item">
          <a class="nav-link js-dark-toggle" href="#"><i class="fas fa-moon" aria-hidden="true"></i></a>
        </li>
        

      </ul>
    </div>
  </div>
</nav>


  <div class="pub" itemscope itemtype="http://schema.org/CreativeWork">

  













<div class="container split-header">
  <div class="row justify-content-center">
    <div class="col-lg-8">
        <img class="img-fluid w-100" src="/publication/2022_fitness/featured_hu7b0e4130b559fec08c970581fa054566_47468_680x500_fill_q90_lanczos_smart1.jpg" itemprop="image" alt="">
        <span
          class="article-header-caption">Image credit: <a href="/"><strong>Carlos Reding</strong></a></span>
    </div>
    <div class="col-lg-8">
      <h1 itemprop="name">Plasmid carriage and the unorthodox use of Fisher&#39;s theorem in evolutionary biology</h1>

      

      



<meta content="2022-02-22 00:00:00 &#43;0000 UTC" itemprop="datePublished">
<meta content="2022-02-22 00:00:00 &#43;0000 UTC" itemprop="dateModified">

<div class="article-metadata">

  
  
  
  
  <div>
    



  <span itemprop="author name" itemtype="http://schema.org/Person"><a href="/authors/carlos/">Carlos Reding</a></span>

  </div>
  
  

  
  <span class="article-date">
    
    
      
    
    <time>February 22, 2022</time>
  </span>
  

  

  

  
  

  
  

  
    
<div class="share-box" aria-hidden="true">
  <ul class="share">
    
      
      
      
        
      
      
      
      <li>
        <a href="https://twitter.com/intent/tweet?url=https://rc-reding.gitlab.io/publication/2022_fitness/&amp;text=Plasmid%20carriage%20and%20the%20unorthodox%20use%20of%20Fisher&amp;#39;s%20theorem%20in%20evolutionary%20biology" target="_blank" rel="noopener" class="share-btn-twitter">
          <i class="fab fa-twitter"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="mailto:?subject=Plasmid%20carriage%20and%20the%20unorthodox%20use%20of%20Fisher&amp;#39;s%20theorem%20in%20evolutionary%20biology&amp;body=https://rc-reding.gitlab.io/publication/2022_fitness/" target="_blank" rel="noopener" class="share-btn-email">
          <i class="fas fa-envelope"></i>
        </a>
      </li>
    
      
      
      
        
      
      
      
      <li>
        <a href="https://www.linkedin.com/shareArticle?url=https://rc-reding.gitlab.io/publication/2022_fitness/&amp;title=Plasmid%20carriage%20and%20the%20unorthodox%20use%20of%20Fisher&amp;#39;s%20theorem%20in%20evolutionary%20biology" target="_blank" rel="noopener" class="share-btn-linkedin">
          <i class="fab fa-linkedin-in"></i>
        </a>
      </li>
    
  </ul>
</div>


  

</div>

      













<div class="btn-links mb-3">
  
  








  
    
  



<a class="btn btn-outline-primary my-1 mr-1" href="https://www.biorxiv.org/content/10.1101/810259v9.full.pdf" target="_blank" rel="noopener">
  PDF
</a>















<a class="btn btn-outline-primary my-1 mr-1" href="https://doi.org/10.1101/810259" target="_blank" rel="noopener">
  DOI
</a>



</div>


    </div>
    
    </div>
  </div>
</div>


  <div class="article-container">

    
    <h3>Abstract</h3>
    <p class="pub-abstract" itemprop="text">The link between fitness and reproduction rate is a central tenet in evolutionary biology: mutants reproducing faster than the dominant wild-type are favoured by selection, otherwise the mutation is lost. This link is given by Fisher&rsquo;s theorem under the assumption that fitness can only change through mutations. Here I show that fitness, as formalised by Fisher, changes through time without invoking new mutations&mdash;allowing the co-maintenance of fast- and slow-growing genotypes. The theorem does not account for changes in population growth that naturally occur due to resource depletion, but it is key to this unforeseen co-maintenance of growth strategies. To demonstrate that fitness is not constant, as Fisher&rsquo;s theorem predicates, I co-maintained a construct of <em>Escherichia coli</em> harbouring pGW155B, a non-transmissible plasmid that protects against tetracycline, in competition assays without using antibiotics. Despite growing 40% slower than its drug-sensitive counterpart, the construct with pGW155B persisted throughout the competition without tetracyclin&mdash;maintaining the plasmid. Thus, predicting the selection of microbial genotypes may be more difficult than previously thought.</p>
    

    
    <div class="row">
      <div class="col-md-10 mx-auto">
        <div class="row">
          <div class="col-12 col-md-3 pub-row-heading">Type</div>
          <div class="col-12 col-md-9">
            
            
            <a href="/publication/#3">
              Preprint
            </a>
            
          </div>
        </div>
      </div>
    </div>
    <div class="d-md-none space-below"></div>
    

    
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-12 col-md-3 pub-row-heading">Publication</div>
          <div class="col-12 col-md-9"><em>bioRxiv</em> (undergoing peer-review)</div>
        </div>
      </div>
      <div class="col-md-1"></div>
    </div>
    <div class="d-md-none space-below"></div>
    

    <div class="space-below"></div>

    <div class="article-style"><!-- raw HTML omitted -->
<!-- raw HTML omitted -->
<p><strong>Currently undergoing peer-review.</strong></p>
</div>

    

<div class="article-tags">
  
  <a class="badge badge-light" href="/tags/mathematical-modelling/">Mathematical modelling</a>
  
  <a class="badge badge-light" href="/tags/microbial-communities/">Microbial communities</a>
  
  <a class="badge badge-light" href="/tags/biophysics/">Biophysics</a>
  
</div>


    








  
  
    
  
  





  
  
  
    
  
  
  <div class="media author-card" itemscope itemtype="http://schema.org/Person">
    

    <div class="media-body">
      <h5 class="card-title" itemprop="name"><a href="https://rc-reding.gitlab.io/">Carlos Reding</a></h5>
      <h6 class="card-subtitle">Senior Bioinformatician</h6>
      <p class="card-text" itemprop="description">My research interests include biosensors, microbial evolution, and molecular biology.</p>
      <ul class="network-icon" aria-hidden="true">
        
          
          
          
            
          
          
          
          
          
            
          
          <li>
            <a itemprop="sameAs" href="https://www.linkedin.com/in/rc-reding/" target="_blank" rel="noopener">
              <i class="fab fa-linkedin"></i>
            </a>
          </li>
        
          
          
          
            
          
          
          
          
          
            
          
          <li>
            <a itemprop="sameAs" href="https://twitter.com/rc_reding" target="_blank" rel="noopener">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
        
          
          
          
            
          
          
          
          
          
            
          
          <li>
            <a itemprop="sameAs" href="https://scholar.google.co.uk/citations?user=5MlVEr4AAAAJ" target="_blank" rel="noopener">
              <i class="fab fa-google"></i>
            </a>
          </li>
        
          
          
          
            
          
          
          
          
          
            
          
          <li>
            <a itemprop="sameAs" href="https://gitlab.com/rc-reding" target="_blank" rel="noopener">
              <i class="fab fa-gitlab"></i>
            </a>
          </li>
        
      </ul>
    </div>
  </div>




  </div>
</div>



      

    
    

    
    
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js" integrity="sha256-lqvxZrPLtfffUl2G/e7szqSvPBILGbwmsGE1MKlOi0Q=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js" integrity="sha256-CBrpuqrMhXwcLLUd5tvQ4euBHCdh7wGlDfNz8vbu/iI=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js" integrity="sha256-X5PoE3KU5l+JcX+w09p/wHl9AzK333C4hJ2I9S5mD4M=" crossorigin="anonymous"></script>

      

      
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js" integrity="sha256-aYTdUrn6Ow1DDgh5JTc3aDGnnju48y/1c8s1dgkYPQ8=" crossorigin="anonymous"></script>
        
      

      
      
    

    
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/leaflet.js" integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log==" crossorigin="anonymous"></script>
    

    
    
    

    
    
    <script>hljs.initHighlightingOnLoad();</script>
    

    

    
    

    

    
    

    
    

    
    
    
    
    
    
    
    
    
    
    
    
    <script src="/js/academia.min.6c2ba2801d406881b3c2277043cedd76.js"></script>

    






  
  <div class="container">
    <footer class="site-footer">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6 mb-4 mb-md-0">
        
        <p class="mb-0">
          Copyright © 2024 &middot; 
          Powered by
          <a href="https://gethugothemes.com" target="_blank" rel="noopener">Gethugothemes</a>
        </p>
      </div>
      <div class="col-md-6">
        <ul class="list-inline network-icon text-right mb-0">
          
          
          
          
          
          
          
          
          
          
          
          
          
          <li class="list-inline-item">
            <a href="https://twitter.com/rc_reding" target="_blank" rel="noopener" title="Say hi"><i class="fab fa-twitter" aria-hidden="true"></i></a>
          </li>
          
          
          
          
          
          
          
          
          
          
          
          
          <li class="list-inline-item">
            <a href="https://mstdn.science/@rc_reding" target="_blank" rel="noopener" title="Say hi"><i class="fab fa-mastodon" aria-hidden="true"></i></a>
          </li>
          
          
          
          
          
          
          
          
          
          
          
          
          <li class="list-inline-item">
            <a href="https://www.linkedin.com/in/rc-reding/" target="_blank" rel="noopener" title="Say hi"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
          </li>
          
        </ul>
      </div>
    </div>
  </div>
</footer>
  </div>
  

  
<div id="modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Cite</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <pre><code class="tex hljs"></code></pre>
      </div>
      <div class="modal-footer">
        <a class="btn btn-outline-primary my-1 js-copy-cite" href="#" target="_blank">
          <i class="fas fa-copy"></i> Copy
        </a>
        <a class="btn btn-outline-primary my-1 js-download-cite" href="#" target="_blank">
          <i class="fas fa-download"></i> Download
        </a>
        <div id="modal-error"></div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
