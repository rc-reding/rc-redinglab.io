---
# Display name
name: Carlos Reding
avatar_image: "you_shouldnt_download_this.jpg"
# Username (this should match the folder name)
authors:
- carlos
# resume download button
btn:
- url : "files/Reding_CV.pdf"
  label : "Download CV"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Senior Bioinformatician

# Organizations/Affiliations
organizations:
- name: University of Oxford
  url: "https://users.ox.ac.uk/~clme2688/"

# Short bio (displayed in user profile at end of posts)
bio: "My research interests include biosensors,
      microbial evolution, and molecular biology."

# Should the user's education and interests be displayed?
display_education: false

interests:
- Modelling of complex systems
- Microbial Evolution
- Regulatory networks
- Electronic Engineering

education:
  courses:
  - course: PhD in Quantitative Biology
    institution: University of Exeter (UK)
    year: 2016
  - course: Licenciatura en Biología
    institution: Universidad de Málaga (Spain)
    year: 2010

# Social/academia Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: 'https://www.linkedin.com/in/rc-reding/'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/rc_reding
- icon: google
  icon_pack: fab
  link: https://scholar.google.co.uk/citations?user=5MlVEr4AAAAJ
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/rc-reding
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
#- icon: cv
#  icon_pack: ai
#  link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
#- Visitors
---
Carlos is a senior bioinformatician at the Nuffield Department of Medicine, 
University of Oxford (UK). His research interests include modelling of living 
systems, microbial regulatory networks, and development of quantitative
methods, all aimed at studying microbial diversity and evolution.

After completing his PhD in Prof [Beardmore's](https://rebear217.github.io/home.html)
laboratory in Exeter (UK), and postdocs in [Lars Steinmetz's](https://web.stanford.edu/group/steinmetzlab/cgi-bin/wordpress/) 
laboratory in Stanford (USA) and [Matthew Avison's](https://research-information.bris.ac.uk/en/persons/matthew-b-avison)
laboratory in Bristol (UK), Carlos joined the [Modernising Medical Microbiology](https://www.expmedndm.ox.ac.uk/modernising-medical-microbiology)
research group. The group is led by Profs [Derrick Crook](https://www.expmedndm.ox.ac.uk/team/derrick-crook),
[Tim Peto](https://www.expmedndm.ox.ac.uk/team/tim-peto) and 
[Sarah Walker](https://www.expmedndm.ox.ac.uk/team/ann-sarah-walker),
and Carlos will develop a novel pipeline to help bridge the gap between 
genomics and the diagnosis of bacterial infections in the clinic. 
