---
title: AMR predictions from genomic datasets

summary: "Writing algorithms to screen through
	  2,000+ farm and human isolates and
	  predict antibiotic resistance from their
	  genomes---without requiring a reference
	  genome."

tags:
- Algorithm development
- Genomics
date: "2022-04-25T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by [**Carlos Reding**](#)
  focal_point: Smart

links:
#- icon: mastodon
#  icon_pack: fab
#  name: Follow
#  url: https://scholar.social/@rc_reding
url_code: https://gitlab.com/rc-reding/software/-/tree/main/Hound
url_pdf: https://www.biorxiv.org/content/10.1101/2023.09.15.557405v1.full.pdf
#url_slides: "files/2018_GE_SB_Workshop.pdf"
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
