---
title: Exploiting the electrical properties of RNA/DNA 

summary: "Development of milifluidic impedance biosensors
	  to detect the presence of SARS-CoV-2 RNA molecules
	  from human samples. "

tags:
- Technology development
- Algorithm development
- Molecular biology
date: "2020-10-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by [**Carlos Reding**](#)
  focal_point: Smart

links:
#- icon: mastodon
#  icon_pack: fab
#  name: Follow
#  url: https://scholar.social/@rc_reding
# url_code: ""
#url_pdf: ""
#url_slides: "files/2018_GE_SB_Workshop.pdf"
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
