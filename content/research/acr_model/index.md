---
title: Regulation of multi-drug efflux pump AcrAB-TolC
summary: "The abundance of AcrAB-TolC protein seldom correlates
	 with the number copies of _acr_ operons in _Escherichia
	 coli_. The regulation of this efflux pump is complex, so
	 to understand this phenomenon I modelled its entire
	 regulatory network."

tags:
- Mathematical modelling
- Regulatory networks
- Antibiotic resistance
date: "2018-04-26T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by [**Carlos Reding**](#)
  focal_point: Smart

links:
#- icon: mastodon
#  icon_pack: fab
#  name: Follow
#  url: https://scholar.social/@rc_reding
# url_code: ""
#url_pdf: ""
url_slides: "files/2018_GE_SB_Workshop.pdf"
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
