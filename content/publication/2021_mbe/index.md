---
title: "The Antibiotic Dosage of Fastest Resistance Evolution: 
Gene Amplifications Underpinning the Inverted-U"

authors:
- carlos
- Pablo Catalán
- Gunther Jansen
- Tobias Bergmiller
- Emily Wood
- Phillip Rosenstiel
- Hinrich Schulenburg
- Ivana Gudelj
- Robert Beardmore

date: "2021-03-08T00:00:00Z"
doi: "10.1093/molbev/msab025"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Molecular Biology and Evolution,* **38**(9):3847--3863"
publication_short: "*Mol. Biol. Evol.,* **38**(9):3847--3863"

abstract: "To determine the dosage at which antibiotic resistance 
	   evolution is most rapid, we treated *Escherichia coli* in 
	   vitro, deploying the antibiotic erythromycin at dosages 
	   ranging from zero to high. Adaptation was fastest just 
	   below erythromycin’s minimal inhibitory concentration 
	   (MIC) and genotype-phenotype correlations determined from 
	   whole genome sequencing revealed the molecular basis: 
	   simultaneous selection for copy number variation in three 
	   resistance mechanisms which exhibited an “inverted-U” 
	   pattern of dose-dependence, as did several insertion 
	   sequences and an integron. Many genes did not conform to 
	   this pattern, however, reflecting changes in selection as 
	   dose increased: putative media adaptation polymorphisms 
	   at zero antibiotic dosage gave way to drug target 
	   (ribosomal RNA operon) amplification at mid dosages 
	   whereas prophage-mediated drug efflux amplifications 
	   dominated at the highest dosages. All treatments exhibited 
	   *E. coli* increases in the copy number of efflux operons 
	   *acrAB* and *emrE* at rates that correlated with increases 
	   in population density. For strains where the inverted-U 
	   was no longer observed following the genetic manipulation 
	   of *acrAB*, it could be recovered by prolonging the 
	   antibiotic treatment at subMIC dosages."

# Summary. An optional shortened abstract.
summary: "The Mutant Selection Window (MSW) claims that selection for
	  resistance begins only after the minimum inhibitory
	  concentration, but there is no rationale behind it and no
	  data to support it. Here we exposed *E. coli* to different
	  antibiotic concentrations to measure which leads to fastest
	  adaptation, and underpin the genetic mechanism."


tags:
- Microbial evolution
- Antibiotic resistance
- Efflux pumps
- Genomics
featured: false

links:
# - name: ""
#   url: ""
url_pdf: https://academic.oup.com/mbe/article-pdf/38/9/3847/39883150/msab025.pdf
# url_code: ''
url_dataset: 'https://www.ebi.ac.uk/ena/browser/view/PRJEB28068'
url_poster: 'files/Poster_Leicester.pdf'
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Carlos Reding**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

