---
title: "The unconstrained evolution of fast and efficient antibiotic-resistant bacterial genomes"
authors:
- carlos
- Mark Hewlett
- Sarah Duxbury
- Fabio Gori
- Ivana Gudelj
- Robert Beardmore

date: "2017-01-30T00:00:00Z"
doi: "10.1038/s41559-016-0050"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Nature Ecology & Evolution,* 0050"
publication_short: "*Nat. Ecol. Evol.,* 0050"

abstract: "Evolutionary trajectories are constrained by trade-offs 
	   when mutations that benefit one life history trait incur 
	   fitness costs in other traits. As resistance to 
	   tetracycline antibiotics by increased efflux can be 
	   associated with an increase in length of the *Escherichia 
	   coli* chromosome of 10% or more, we sought costs of 
	   resistance associated with doxycycline. However, it was 
	   difficult to identify any because the growth rate (r), 
	   carrying capacity (K) and drug efflux rate of *E. coli* 
	   increased during evolutionary experiments where the 
	   species was exposed to doxycycline. Moreover, these 
	   improvements remained following drug withdrawal. We sought 
	   mechanisms for this seemingly unconstrained adaptation, 
	   particularly as these traits ought to trade-off according 
	   to rK selection theory. Using prokaryote and eukaryote 
	   microorganisms, including clinical pathogens, we show that 
	   r and K can trade-off, but need not, because of ‘rK 
	   trade-ups’. r and K trade-off only in sufficiently 
	   carbon-rich environments where growth is inefficient. We 
	   then used *E. coli* ribosomal RNA (rRNA) knockouts to 
	   determine specific mutations, namely changes in rRNA operon 
	   (*rrn*) copy number, than can simultaneously maximize r and 
	   K. The optimal genome has fewer operons, and therefore 
	   fewer functional ribosomes, than the ancestral strain. It 
	   is, therefore, unsurprising for r-adaptation in the 
	   presence of a ribosome-inhibiting antibiotic, doxycycline, 
	   to also increase population size. We found two costs for 
	   this improvement: an elongated lag phase and the loss of 
	   stress protection genes."

# Summary. An optional shortened abstract.
summary: "For decades it was hypothesised that the growth rate of
	  populations and their size engage in a trade-off, but the
	  data was always inconclusive. Using bacteria, we set off to
	  underpin a physical mechanism for this trade-off, and then
	  explain why it is not always found in the data."


tags:
- Mathematical modelling
- Population genetics
- Microbial evolution

featured: true

links:
# - name: ""
#   url: ""
url_pdf: 'files/2017_Reding.pdf'
# url_code: ''
# url_dataset: ''
# url_poster: ''
# url_project: ''
url_slides: 'files/Presentation_IBECC_v3_Final.pdf'
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Carlos Reding**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

