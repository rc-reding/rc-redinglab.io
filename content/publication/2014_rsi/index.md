---
title: "Testing the optimality properties of a dual antibiotic treatment in a two-locus, two-allele model"

authors:
- Rafael Peña Miller
- Ayari Fuentes Hernández
- carlos
- Ivana Gudelj
- Robert Beardmore

date: "2014-04-15T00:00:00Z"
doi: "10.1098/rsif.2013.1035"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Journal of the Royal Society Interface,* **11**: 20131035."
publication_short: "*J. R. Soc. Interface* **11**: 20131035."

abstract: Mathematically speaking, it is self-evident that the optimal control of complex, dynamical systems with many interacting components cannot be achieved with ‘non-responsive’ control strategies that are constant through time. Although there are notable exceptions, this is usually how we design treatments with antimicrobial drugs when we give the same dose and the same antibiotic combination each day. Here, we use a frequency- and density-dependent pharmacogenetics mathematical model based on a standard, two-locus, two-allele representation of how bacteria resist antibiotics to probe the question of whether optimal antibiotic treatments might, in fact, be constant through time. The model describes the ecological and evolutionary dynamics of different sub-populations of the bacterium *Escherichia coli* that compete for a single limiting resource in a two-drug environment. We use *in vitro* evolutionary experiments to calibrate and test the model and show that antibiotic environments can support dynamically changing and heterogeneous population structures. We then demonstrate, theoretically and empirically, that the best treatment strategies should adapt through time and constant strategies are not optimal.

# Summary. An optional shortened abstract.
#summary: Lorem iperat. Proin tincidunt magna sed ex sollicitudin condimentum

tags:
- Mathematical modelling
- Microbial evolution
- Antibiotic resistance
featured: false

links:
# - name: ""
#   url: ""
url_pdf: https://royalsocietypublishing.org/doi/pdf/10.1098/rsif.2013.1035
# url_code: ''
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Rafael Peña**](http://www.penamiller.com/lab/)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

