---
title: "Regeneración retiniana asociada a hidrocefalia congénita debida a mutación en el gen que codifica alfa-SNAP"

authors:
- Antonio Jiménez
- Diana Boomgard
- Patricia Rivera
- carlos
- José Pérez Fígares

date: "2005-09-01T00:00:00Z"
doi: "10.33588/rn.41S02.2005758"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["1"]

# Publication name and optional abbreviated publication name.
publication: "*Revista de Neurolog\'{i}a,* **41** (Supl 2): 64."
publication_short: "*Rev. Neurol.* **41** (Supl 2): 64."

abstract: "En el ratón mutante _hyh_ existen bajos niveles funcionales de
	   la proteína alfa-SNAP para interaccionar con el complejo SNARE
	   implicado en el tráfico vesicular intracelular, afectándose el 
	   proceso de secreción y adhesión celular durante al desarrollo 
	   del sistema nervioso central. Consecuentemente, los ratones
	   mutantes manifiestan hidrocefalia congénita por desprendimiento 
	   del neuroepitelio, probablemente debido a alteraciones en la 
	   adhesión. Algunos síndromes humanos presentan hidrocefalia 
	   asociada a defectos retinianos, como el síndrome de Walker-Waburg, 
	   con displasia retiniana. La hidrocefalia del ratón _hyh_ ha 
	   demostrado ser un modelo representativo de la humana, y por eso 
	   el estudio de su degeneración retiniana asociada puede arrojar 
	   luz sobre defectos parecidos en la hidrocefalia humana. Se han 
	   estudiado las retinas de ratones mutantes con la enfermedad 
	   cronificada que han podido sobrevivir hasta edades avanzadas. 
	   Las retinas de ratones normales y mutantes, desde el nacimiento 
	   hasta 180 días postnatales, se procesaron para reconocer sus 
	   distintos tipos celulares e identificar los que mueren. Se estudió
	   la expresión de alfa-SNAP y la implicación del citoesqueleto y 
	   moléculas de adhesión en la degeneración, así como el papel que 
	   desempeña en el proceso degenerativo la microglía/macrófagos y 
	   los astrocitos. Se encontraron diversos defectos en las retinas
	   de los animales hidrocefálicos, consistentes en muertes de 
	   fotorreceptores y otros tipos celulares, destacando la ausencia 
	   casi total de conos y bastones y de la capa plexiforme externa."


# Summary. An optional shortened abstract.
#summary: Lorem iperat. Proin tincidunt magna sed ex sollicitudin condimentum

tags:
- Cell biology
- Hydrocephaly
featured: false

links:
# - name: ""
#   url: ""
url_pdf: "files/2005_Jimenez.pdf"
# url_code: ''
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Rafael Peña**](http://www.penamiller.com/lab/)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

