---
title: "Harvesting and amplifying gene cassettes 
				confers cross-resistance to critically important antibiotics"

authors:
- Punyawee Dulyayangkul
- Thomas Beavis
- Winnie WY Lee
- Robbie Ardagh
- Frances Edwards
- Fergus Hamilton
- Ian Head
- Kate J. Heesom
- Oliver Mounsey
- Marek Murarik
- carlos
- Naphat Satapoomin
- John M. Shaw
- Yuiko Takebayashi
- Catherine L. Tooke
- James Spencer
- Philip B. Williams
- Matthew B. Avison

date: "2024-06-06T00:00:00Z"
doi: "10.1371/journal.ppat.1012235"

# Schedule page publish date (NOT publication's date).
publishDate: "2024-06-06T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*PLoS Pathogens* **20**(6): e1012235"
publication_short: "*PLoS Pathog.* **20**(6): e1012235"

abstract: "Amikacin and piperacillin/tazobactam are frequent antibiotic
					 choices to treat bloodstream infection, which is commonly fatal
					and most often caused by bacteria from the family _Enterobacterales_.
					Here we show that two gene cassettes located side-by-side in and
					ancestral integron similar to _In37_ have been “harvested” by
					insertion sequence _IS26_ as a transposon that is already globally
					disseminated among the _Enterobacterales_. This transposon encodes
					the enzymes AAC(6’)-Ib-cr and OXA-1, reported, respectively, as 
					amikacin and piperacillin/tazobactam resistance mechanisms. However, 
					by studying bloodstream infection isolates from 769 patients from 
					three hospitals serving a population of 1.5 million people in South
					West England, we show that increased enzyme production due to mutation
					in an _IS26_/_In37_-derived hybrid promoter or, more commonly,
					transposon copy number amplification is required to simultaneously
					remove these two key therapeutic options; in many cases leaving only
					the last-resort antibiotic, meropenem. These findings may help improve
					the accuracy of predicting piperacillin/tazobactam treatment failure,
					allowing stratification of patients to receive meropenem or
					piperacillin/tazobactam, which may improve outcome and slow the
					emergence of meropenem resistance."

# Summary. An optional shortened abstract.
# summary: ""

tags:
- Blood Stream Infection
- Bioinformatics
- Epidemiology
featured: false

links:
# - name: ""
# url: "https://www.biorxiv.org/content/10.1101/2023.06.15.545115v1"
url_pdf: https://journals.plos.org/plospathogens/article/file?id=10.1371/journal.ppat.1012235&type=printable
# url_code: 'https://github.com/rc-reding/papers/tree/master/MicrobCommunities_ISMECOMMS_2022'
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Punyawee Dulyayangkul**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: [2023_hound]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

