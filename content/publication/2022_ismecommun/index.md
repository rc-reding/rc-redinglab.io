---
title: "Predicting the re‐distribution of antibiotic molecules caused by
	inter‐species interactions in microbial communities"

authors:
- carlos

date: "2022-11-05T00:00:00Z"
doi: "10.1038/s43705-022-00186-5"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*ISME Communications,* **2**:110"
publication_short: "*ISME Commun.,* **2**:110"

abstract: "Microbes associate in nature forming complex communities, but 
	   they are often studied in purified form. Here I show that
	   neighbouring species enforce the re‐distribution of carbon 
	   and antimicrobial molecules, predictably changing drug 
	   efficacy with respect to standard laboratory assays. A simple 
	   mathematical model, validated experimentally using pairwise 
	   competition assays, suggests that differences in drug 
	   sensitivity between the competing species causes the
	   re-distribution of drug molecules without affecting carbon 
	   uptake. The re‐distribution of drug is even when species have 
	   similar drug sensitivity, reducing drug efficacy. But when 
	   their sensitivities differ the re‐distribution is uneven: The 
	   most sensitive species accumulates more drug molecules, 
	   increasing efficacy against it. Drug efficacy tests relying 
	   on samples with multiple species are considered unreliable and 
	   unpredictable, but study demonstrates that efficacy in these 
	   cases can be qualitatively predicted. It also suggests that 
	   living in communities can be beneficial even when all species 
	   compete for a single carbon source, as the relationship between 
	   cell density and drug required to inhibit their growth may be 
	   more complex than previously thought."

# Summary. An optional shortened abstract.
summary: "Drug efficacy measured using pure cultures is not maintained in
	  the presence of neighbouring microbes. Using simple physical laws,
	  I pinpoint the mechanism for this change to demonstrate that 
	  drug efficacy can be predictably manupulated, and suggest why
	  microbes began to co-operate and form communities."


tags:
- Mathematical modelling
- Microbial communities
- Biophysics
featured: true

links:
# - name: ""
#   url: ""
url_pdf: https://www.nature.com/articles/s43705-022-00186-5.pdf
url_code: 'https://gitlab.com/rc-reding/papers/-/tree/master/MicrobCommunities_ISMECOMMS_2022'
#url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Carlos Reding**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

