---
title: "Plasmid carriage and the unorthodox use of
	Fisher's theorem in evolutionary biology"

authors:
- carlos

date: "2022-02-22T00:00:00Z"
doi: "10.1101/810259"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "_bioRxiv_ (undergoing peer-review)"
publication_short: "_bioRxiv_ (undergoing peer-review)"

abstract: "The link between fitness and reproduction 
	   rate is a central tenet in evolutionary 
	   biology: mutants reproducing faster than 
	   the dominant wild-type are favoured by 
	   selection, otherwise the mutation is lost. 
	   This link is given by Fisher's theorem 
	   under the assumption that fitness can only 
	   change through mutations. Here I show that 
	   fitness, as formalised by Fisher, changes 
	   through time without invoking new 
	   mutations---allowing the co-maintenance of 
	   fast- and slow-growing genotypes. The 
	   theorem does not account for changes in 
	   population growth that naturally occur due 
	   to resource depletion, but it is key to this 
	   unforeseen co-maintenance of growth 
	   strategies. To demonstrate that fitness is 
	   not constant, as Fisher's theorem predicates, 
	   I co-maintained a construct of _Escherichia
	   coli_ harbouring pGW155B, a non-transmissible 
	   plasmid that protects against tetracycline, 
	   in competition assays without using 
	   antibiotics. Despite growing 40% slower than 
	   its drug-sensitive counterpart, the construct 
	   with pGW155B persisted throughout the 
	   competition without tetracyclin---maintaining 
	   the plasmid. Thus, predicting the selection 
	   of microbial genotypes may be more difficult 
	   than previously thought."


# Summary. An optional shortened abstract.
summary: "" 

tags:
- Mathematical modelling
- Microbial communities
- Biophysics
featured: false

links:
# - name: ""
#   url: ""
url_pdf: https://www.biorxiv.org/content/10.1101/810259v9.full.pdf
#  url_code: 'https://github.com/rc-reding/papers/tree/master/MicrobCommunities_ISMECOMMS_2022'
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Carlos Reding**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

**Currently undergoing peer-review.**
