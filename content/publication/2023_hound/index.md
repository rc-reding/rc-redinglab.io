---
title: "Hound: A novel tool for automated mapping of genotype to phenotype
				in bacterial genomes assembled de novo"

authors:
- carlos
- Naphat Satapoomin
- Matthew B. Avison

date: "2024-02-21T00:00:00Z"
doi: "10.1093/bib/bbae057"

# Schedule page publish date (NOT publication's date).
publishDate: "2024-02-21T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Brief. Bioinform.* **25**(2), 1--10"
publication_short: "*Brief. Bioinform.* **25**(2), 1--10"

abstract: "Increasing evidence suggests that microbial species have a 
					 strong within species genetic heterogeneity. This can be 
					 problematic for the analysis of prokaryote genomes, which 
					 commonly relies on a reference genome to guide the assembly 
					 process: any difference between reference and sample 
					 genomes can introduce errors in the detection of small 
					 insertions, deletions, structural variations and even point 
					 mutations. This phenomenon jeopardises the genomic 
					 surveillance of antibiotic-resistant bacteria, triggering 
					 even a reproducibility crisis. Here we present Hound, an 
					 analysis pipeline that integrates publicly available tools 
					 to locally assemble prokaryote genomes _de novo_, detect 
					 genes by similarity, and report the mutations found. Three 
					 features are exclusive to Hound: It reports relative gene 
					 copy number, retrieves sequences upstream the start codon 
					 to detect mutations in promoter regions, and, importantly, 
					 can merge contigs based on a user-given query sequence to 
					 reconstruct genes that are fragmented by the assembler. To 
					 demonstrate Hound, we screened through 5,032 bacterial 
					 whole-genome sequences isolated from farm animals and 
					 clinical patients using the amino acid sequence of _blaTEM-1_, 
					 to predict resistance to amoxicillin-clavulanate. We believe 
					 this tool can facilitate the analysis of prokaryote species 
					 that currently lack a reference genome, and can be scaled up 
					 to build automated diagnostic systems."

# Summary. An optional shortened abstract.
#summary: "Drug efficacy measured using pure cultures is not maintained in"


tags:
- Bioinformatics
featured: false

links:
# - name: ""
# url: ""
url_pdf: https://academic.oup.com/bib/article-pdf/25/2/bbae057/56728727/bbae057.pdf
url_code: 'https://gitlab.com/rc-reding/software/-/tree/main/Hound'
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Carlos Reding**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

