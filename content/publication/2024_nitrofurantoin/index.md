---
title: "Improving Nitrofurantoin Resistance
			 Prediction in Escherichia coli from
			 Whole Genome Sequence by Integrating NfsA/B Enzyme Assays"

authors:
- Punyawee Dulyayangkul
- Jordan E Sealey
- Winnie WY Lee
- carlos
- Kate J. Heesom
- Naphat Satapoomin
- Philip B. Williams
- Matthew B. Avison

date: "2024-05-20T00:00:00Z"
doi: "10.1128/aac.00242-24"

# Schedule page publish date (NOT publication's date).
publishDate: "2024-05-20T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Antimicrob. Agents Chemother.* **68**(6):e00242-24"
publication_short: "*Antimicrob. Agents Chemother.* **68**(6):e00242-24"

abstract: "Nitrofurantoin resistance in _Escherichia coli_ is
					primarily caused by mutations damaging two enzymes, 
					NfsA and NfsB. Studies based on small isolate 
					collections with defined nitrofurantoin MICs have 
					found significant random genetic drift in _nfsA_ and 
					_nfsB_ making it extremely difficult to predict 
					nitrofurantoin resistance from whole genome sequence 
					(WGS) where both genes are not obviously disrupted by 
					nonsense or frameshift mutations or insertional 
					inactivation. Here we report a WGS survey of 200 _E. 
					coli_ from community urine samples, of which 34 were 
					nitrofurantoin resistant. We characterised individual 
					non-synonymous mutations seen in _nfsA_ and _nfsB_ 
					among this collection using complementation cloning 
					and assays of NfsA/B enzyme activity in cell extracts.
					We definitively identified R203C, H11Y, W212R, A112E, 
					A112T and A122T in NfsA and R121C, Q142H, F84S, P163H, 
					W46R, K57E and V191G in NfsB as amino acid substitutions 
					that reduce enzyme activity sufficiently to cause 
					resistance. In contrast, E58D, I117T, K141E, L157F, A172S, 
					G187D and A188V in NfsA and G66D, M75I, V93A and A174E in 
					NfsB, are functionally silent in this context. We 
					identified that 9/166 (5.4%) of nitrofurantoin susceptible 
					isolates were pre-resistant, defined as having loss of 
					function mutations in _nfsA_ or _nfsB_. Finally, using 
					NfsA/B enzyme activity assay and proteomics we demonstrated 
					that 9/34 (26.5%) of nitrofurantoin resistant isolates 
					carried functionally wild-type _nfsB_ or _nfsB_/_nfsA_. In 
					these cases, enzyme activity was reduced through 
					downregulated gene expression. Our biological understanding 
					of nitrofurantoin resistance is greatly improved by this 
					analysis, but is still insufficient to allow its reliable 
					prediction from WGS data."

# Summary. An optional shortened abstract.
# summary: ""

tags:
- Urinary Tract Infection
- Bioinformatics
- Enzyme Assays
featured: false

links:
# - name: ""
# url: "https://www.biorxiv.org/content/10.1101/2023.06.15.545115v1"
url_pdf: https://www.biorxiv.org/content/10.1101/2024.01.25.577238v1.full.pdf
# url_code: 'https://github.com/rc-reding/papers/tree/master/MicrobCommunities_ISMECOMMS_2022'
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Punyawee Dulyayangkul**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: [2023_hound]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

