---
title: "Image-based spectrophotometer: The Light-Modulator (LiMO®)"

authors:
- Robert Beardmore
- Ivana Gudelj
- Kai Longshaw
- carlos

date: "2020-02-12T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["8"]

# Publication name and optional abbreviated publication name.
publication: "Intellectual Property Office (United Kingdom)"
publication_short: "IPO"

abstract: "A test apparatus is described comprising a housing
	   (12) containing a location at which a sample tray 
	   (28) containing a plurality of sample wells (30) 
	   can be located, a light box (14) positioned to 
	   permit irradiation of the sample tray (28), and an 
	   imaging arrangement (34) to permit imaging of the 
	   sample tray, wherein the light box (14) comprises 
	   a light source (16) including a light guide (18) in 
	   the form of a plate (20) of a transparent material 
	   and a plurality of light emitting devices (26) 
	   provided adjacent at least one edge of the plate 
	   (20), wherein a surface of the plate (20) is 
	   provided with a plurality of transmitting regions 
	   (22) whereby light is transmitted from the plate (20). 
	   Corresponding methods of calibration and use of the 
	   test apparatus are also described."

# Summary. An optional shortened abstract.
summary:  "We dreamt of phenotyping microbes and running 
	   evolution experiments with antibiotics at much 
	   higher-throughput, using many more strains and 
	   conditions than before.<br>
	   
	   The solution had to be cheap, small, portable and 
	   accurate. So, with [**ERC**](https://cordis.europa.eu/article/id/422425-a-low-cost-sensor-for-monitoring-drug-resistant-bacteria)
	   funding, we set about leveraging low-cost robotics, 
	   adapting some LED lighting technology and using our 
	   own algorithm-building expertise to create the kind of 
	   device we wished we could buy."

tags:
- Optical spectroscopy
- Analysis algorithms
- Technology development

featured: true

links:
- name: "Patent"
  url: https://patents.google.com/patent/WO2020165582A1/en?oq=WO+2020/165582+A1'
- name: "Website"
  url: https://mmems.org/limo/
# url_pdf: https://patentimages.storage.googleapis.com/b5/93/7a/8c3f8d26993fd6/WO2020165582A1.pdf
# url_code: ''
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**BioDynamic Devices**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

