---
title: "Genomic epidemiology of third-generation cephalosporin-resistant 
			  Escherichia coli from Argentinian pig and dairy farms reveals 
				animal-specific patterns of co-resistance and resistance mechanisms"

authors:
- Oliver Mounsey
- Laura Marchetti
- Julián Parada
- Laura  V. Alarcón
- Florencia Aliverti
- Matthew B. Avison
- Carlos S. Ayala
- Cristina Ballesteros
- Caroline M. Best
- Judy Bettridge
- Andrea Buchamer
- Daniel Buldain
- Alicia Carranza
- Maite Cortiisgro
- David Demeritt
- María Paula Escobar
- Lihuel Gortari Castillo
- María Jaureguiberry
- Mariana F. Lucas
- L. Vanina Madoz
- María José Marconi
- Nicolás Moiso
- Hernán D. Nievas
- Marco A. Ramirez Montes De Oca
- carlos
- Kristen K. Reyher
- Lucy Vass
- Sara Williams
- José Giraudo
- R. Luzbel De La Sota
- Nora Mestorino
- Fabiana A. Moredo
- Matías Pellegrino

date: "2024-02-09T13:00:00Z"
doi: "10.1128/aem.01791-23"

# Schedule page publish date (NOT publication's date).
publishDate: "2024-01-09T13:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Appl. Env. Microbiol,.* **90**(1):e01791-23"
publication_short: "*Appl. Env. Microbiol.,* **90**(1):e01791-23"

abstract: "Control measures are being introduced globally to reduce the 
					 prevalence of antibiotic resistant (ABR) bacteria on farms. 
					 However, little is known about the current prevalence and 
					 molecular ecology of ABR in key opportunistic human pathogens 
					 such as _Escherichia coli_ on South American farms. Working 
					 with 30 dairy cattle farms and 40 pig farms across two 
					 provinces in central-eastern Argentina, we report a 
					 comprehensive genomic analysis of third-generation 
					 cephalosporin resistance (3GC-R) in _E. coli_. 3GC-R isolates 
					 were recovered from 34.8% (cattle) and 47.8% (pigs) of samples 
					 from faecally contaminated sites. Phylogenetic analysis 
					 revealed substantial diversity suggestive of long-term 
					 horizontal transmission of 3GC-R mechanisms. Despite this,
					 mechanisms such as CTX-M-15 and CTX-M-2 were detected more 
					 often in dairy farms, while CTX-M-8 and CMY-2, and 
					 co-carriage of amoxicillin/clavulanate resistance and 
					 florfenicol resistance were more commonly detected in pig 
					 farms. This suggests different selective pressures of 
					 antibiotic use in these two animal types, particularly the 
					 balance of fourth-versus third-generation cephalosporin use, 
					 and of amoxicillin/clavulanate and florfenicol use. We 
					 identified the β-lactamase gene blaROB in 3GC-R _E. coli_, 
					 which has previously only been reported in the family 
					 Pasteurellaceae, including farmed animal pathogens. 
					 _blaROB_ was found alongside a novel florfenicol resistance 
					 gene – _ydhC_ – also mobilised from a pig pathogen as part 
					 of a new plasmid-mediated composite transposon, which is 
					 already widely disseminated. These data set a baseline from 
					 which to measure the effects of interventions aimed at 
					 reducing on-farm ABR and provide an opportunity to 
					 investigate zoonotic transmission of resistant bacteria in 
					 this region."

# Summary. An optional shortened abstract.
summary: "Little is known about the ecology of critically important 
					antibiotic resistance among opportunistic human pathogens 
					(e.g. _Escherichia coli_) on South American farms. By 
					studying 70 farms in central-eastern Argentina, we 
					identified that third-generation cephalosporin resistance
					(3GC-R) in _E. coli_ was mediated by mechanisms seen more 
					often in certain species (pigs or dairy cattle) and that 
					3GC-R pig E. coli were more likely to be co-resistant to 
					florfenicol and amoxicillin/clavulanate. This suggests that 
					on-farm antibiotic usage is key to selecting the types of 
					_E. coli_ present on these farms. 3GC-R E. coli were highly 
					phylogenetically variable and we identified the de novo 
					mobilisation of the resistance gene blaROB, alongside a 
					novel florfenicol resistance gene, from pig pathogens into 
					_E. coli_ on a mobile genetic element that was widespread 
					in the study region. Overall, this shows the importance of 
					surveying poorly studied regions for critically important 
					antibiotic resistance which might impact human health."


tags:
- One Health
- Bioinformatics
- Epidemiology
featured: false

links:
# - name: ""
# url: "https://www.biorxiv.org/content/10.1101/2023.06.15.545115v1"
url_pdf: https://journals.asm.org/doi/epdf/10.1128/aem.01791-23
# url_code: 'https://github.com/rc-reding/papers/tree/master/MicrobCommunities_ISMECOMMS_2022'
# url_dataset: ''
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Oliver Mounsey**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: [2023_hound]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

