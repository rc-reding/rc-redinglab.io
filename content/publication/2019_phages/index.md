---
title: "Canonical host-pathogen tradeoffs subverted 
	 by mutations with dual benefits"

authors:
- Robert Beardmore
- Mark Hewlett
- Rafael Peña Miller
- carlos
- Ivana Gudelj
- Justin R. Meyer

date: "2019-10-24T00:00:00Z"
doi: "10.1101/818492"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "_bioRxiv_"
publication_short: "_bioRxiv_"

abstract: "Tradeoffs between life history traits 
	   impact diverse biological phenomena, 
	   including the maintenance of biodiversity. 
	   We sought to study two canonical tradeoffs 
	   in a model host-parasite system consisting 
	   of bacteriophage lambda and _Escherichia 
	   coli_: i) parasite resistance for growth 
	   and ii) phage infectivity for host-range. 
	   We report that these previously hypothesised 
	   tradeoffs are, in fact, tradeups. While the 
	   observation of tradeups was surprising, 
	   they should be expected because if traits 
	   X and Y tradeoff, so too traits Y and Z, 
	   then X and Z will tradeup. By considering 
	   five different _E. coli_ trait correlations 
	   we uncovered several tradeups and tradeoffs. 
	   Using mathematical models, we establish that 
	   tradeups need not inhibit biodiversity, as 
	   previously thought, and can help maintain it 
	   through high-dimensional trait interactions. 
	   We provide a mechanistic explanation for how 
	   tradeups emerge and give reasons for why 
	   tradeups can even evolve in well-adapted 
	   genomes."


# Summary. An optional shortened abstract.
summary: ''

tags:
- Mathematical modelling
- Microbial evolution
- Phage resistance
featured: false

links:
# - name: ""
#   url: ""
url_pdf: https://www.biorxiv.org/content/10.1101/818492v1.full.pdf
# url_code: ''
#  url_dataset: 'https://www.ebi.ac.uk/ena/browser/view/PRJEB28068'
#  url_poster: 'files/Poster_Leicester.pdf'
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Robert Beardmore**](http://people.exeter.ac.uk/reb217/rebHomePage/home.html)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

