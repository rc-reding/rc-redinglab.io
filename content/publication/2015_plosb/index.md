---
title: "Using a Sequential Regimen to Eliminate Bacteria at Sublethal Antibiotic Dosages"
authors:
- Ayari Fuentes Hernández
- Jessica Plucain
- Fabio Gori
- Rafael Peña Miller
- carlos
- Gunther Jansen
- Hinrich Schulenburg
- Ivana Gudelj
- Robert Beardmore

date: "2015-04-08T00:00:00Z"
doi: "10.1371/journal.pbio.1002104"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*PLoS Biology,* **13**(4)"
publication_short: "*PLoS Biol.,* **13**(4)"

abstract: "We need to find ways of enhancing the potency of existing antibiotics, and, with this in mind, we begin with an unusual question: how low can antibiotic dosages be and yet bacterial clearance still be observed? Seeking to optimise the simultaneous use of two antibiotics, we use the minimal dose at which clearance is observed in an in vitro experimental model of antibiotic treatment as a criterion to distinguish the best and worst treatments of a bacterium, *Escherichia coli*. Our aim is to compare a combination treatment consisting of two synergistic antibiotics to so-called sequential treatments in which the choice of antibiotic to administer can change with each round of treatment. Using mathematical predictions validated by the *E. coli* treatment model, we show that clearance of the bacterium can be achieved using sequential treatments at antibiotic dosages so low that the equivalent two-drug combination treatments are ineffective. Seeking to treat the bacterium in testing circumstances, we purposefully study an *E. coli* strain that has a multidrug pump encoded in its chromosome that effluxes both antibiotics. Genomic amplifications that increase the number of pumps expressed per cell can cause the failure of high-dose combination treatments, yet, as we show, sequentially treated populations can still collapse. However, dual resistance due to the pump means that the antibiotics must be carefully deployed and not all sublethal sequential treatments succeed. A screen of 136 96-h-long sequential treatments determined five of these that could clear the bacterium at sublethal dosages in all replicate populations, even though none had done so by 24 h. These successes can be attributed to a collateral sensitivity whereby cross-resistance due to the duplicated pump proves insufficient to stop a reduction in *E. coli* growth rate following drug exchanges, a reduction that proves large enough for appropriately chosen drug switches to clear the bacterium."

# Summary. An optional shortened abstract.
summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- Mathematical modelling
- Antibiotic resistance

featured: false

# links:
# - name: ""
#   url: ""
url_pdf: https://journals.plos.org/plosbiology/article/file?id=10.1371/journal.pbio.1002104&type=printable
# url_code: ''
url_dataset: 'https://www.ebi.ac.uk/ena/browser/view/PRJEB7832'
# url_poster: ''
# url_project: ''
# url_slides: ''
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Ayari Fuentes**](https://www.ccg.unam.mx/en/ayari-fuentes-hernandez/)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

