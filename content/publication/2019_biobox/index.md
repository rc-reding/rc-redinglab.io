---
title: "Fluorescence photography of patterns and 
	waves of bacterial adaptation at high 
	antibiotic doses"

authors:
- carlos
- Mark Hewlett
- Tobias Bergmiller
- Ivana Gudelj
- Robert Beardmore

date: "2019-10-16T00:00:00Z"
doi: "10.1101/806232"

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "_bioRxiv_"
publication_short: "_bioRxiv_"

abstract: "Fisher suggested advantageous genes would 
	   spread through populations as a wave so we 
	   sought genetic waves in evolving 
	   populations, as follows. By fusing a 
	   fluorescent marker to a drug efflux protein 
	   (AcrB) whose expression provides 
	   _Escherichia coli_ with resistance to some 
	   antibiotics, we quantified the evolution and 
	   spread of drug-resistant _E. coli_ through 
	   spacetime using image analysis and 
	   quantitative PCR. As is done in hospitals 
	   routinely, we exposed the bacterium to a 
	   gradient of antibiotic in a ‘disk diffusion’ 
	   drug susceptibility test that we videoed. 
	   The videos show complex spatio-genomic 
	   patterns redolent of, yet more complex than, 
	   Fisher’s predictions whereby a decelerating 
	   wave front of advantageous genes colonises 
	   towards the antibiotic source, forming 
	   bullseye patterns en route and leaving a 
	   wave back of bacterial sub-populations 
	   expressing _acrB_ at decreasing levels 
	   away from the drug source. qPCR data show 
	   that _E. coli_ sited at rapidly-adapting 
	   spatial hotspots gain 2 additional copies 
	   of _acr_, the operon that encodes AcrB, 
	   within 24h and imaging data show resistant 
	   sub-populations thrive most near the 
	   antibiotic source due to non-monotone 
	   relationships between inhibition due to 
	   antibiotic and distance from the source. 
	   In the spirit of Fisher, we provide an 
	   explicitly spatial nonlinear diffusion 
	   equation that exhibits these properties 
	   too. Finally, linear diffusion theory 
	   quantifies how the spatial extent of 
	   bacterial killing scales with increases 
	   in antibiotic dosage, predicting that 
	   microbes can survive chemotherapies that 
	   have been escalated to 250× the clinical 
	   dosage if the antibiotic is 
	   diffusion-limited."


# Summary. An optional shortened abstract.
summary: ''

tags:
- Mathematical modelling
- Microbial evolution
- Analysis algorithm
- Technology development

featured: false

links:
# - name: ""
#   url: ""
url_pdf: https://www.biorxiv.org/content/10.1101/806232v1.full.pdf
# url_code: ''
#  url_dataset: 'https://www.ebi.ac.uk/ena/browser/view/PRJEB28068'
url_poster: 'files/Poster_Hinxton.pdf'
# url_project: ''
url_slides: 'files/2017_Zeeman.pdf'
# url_source: ''
# url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Carlos Reding**](#)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo academia's Markdown slides feature.
{{% /alert %}}

